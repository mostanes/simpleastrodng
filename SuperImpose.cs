﻿using System;
using System.Collections.Generic;
using Hjg.Pngcs;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;

namespace DNGReader
{
	public class SuperImpose
	{
		public List<Transform> trns;
		public List<DNGPixmap> pxms;
		uint Width, Height;
		PngWriter pngw;
		ImageInfo imi;
		public const int ThrowOut = 700;
		public const double WB_R = 1.300;
		public const double WB_G = 1.000;
		public const double WB_B = 1.741;
		public const double WB_Norm = 3 / (WB_R + WB_G + WB_B);
		public SuperImpose ()
		{
		}

		public SuperImpose(uint width, uint height, string outname)
		{
			imi = new ImageInfo ((int)width, (int)height, 16, false, false, false);
			pngw = FileHelper.CreatePngWriter (outname, imi, true);
			Width = width;
			Height = height;



			/*
			for (int col = 0; col < imi.Cols; col++) {
				int r = 255;
				int g = 127;
				int b = 255 * col / imi.Cols;
				ImageLineHelper.SetPixel(iline , col, r, g, b); // orange-ish gradient
			}
			for (int row = 0; row < png.ImgInfo.Rows; row++) {
				png.WriteRow(iline, row);
			}*/



		}

		public void StartSuperImposing()
		{
			Vector va, vb;
			double halfmask = ImageMask.dark_cu * 3 / 4;
			uint i, j;
			int k, l;
			Console.WriteLine ("Superimposing...");
			double[] vra = new double[pxms.Count];
			double[] vga = new double[pxms.Count];
			double[] vba = new double[pxms.Count];
			double[] hra = new double[(pxms.Count + 1) / 2];
			Bitmap bmp = new Bitmap((int)Width, (int)Height, PixelFormat.Format24bppRgb);
			//double mx, my, mz;
			for (i = 0; i < Height; i++) {
				if (i % 100 == 0)
					Console.Write (i.ToString () + " ");
				//ImageLine imk = new ImageLine (imi);
				for (j = 0; j < Width; j++) {
					int r, g, b;
					double R, G, B;
					int kr=0, kg=0, kb=0;
//					List<double> valr, valg, valb, gg;
//					valr = new List<double> ();
//					valg = new List<double> ();
//					valb = new List<double> ();
//					gg = new List<double> ();
					//double mean;
					va = new Vector ();
					va.xa = 0;
					va.ya = 0;
					va.xb = (int)j;
					va.yb = (int)i;

					for (k = 0; k < pxms.Count; k++) {
						vb = trns [k].TransformVector (va);
						if (vb.xb < 0)
							continue;
						if (vb.yb < 0)
							continue;
						//Color64 c = pxms [k] [(uint)vb.xb, (uint)vb.yb].color;
						ColorStar cs = pxms [k].GetColorStar ((uint)vb.xb, (uint)vb.yb);
						if (cs.AR == 1) {
							vra[kr] = cs.R;
							kr++;
						}
						if (cs.AG == 1) {
							vga[kg] = cs.G;
							kg++;
						}
						if (cs.AB == 1) {
							vba[kb] = cs.B;
							kb++;
						}
					}

					/* Red */
					//mean = 0;
					//meang = 0;
					//R = 0;
					for(k=0; k<hra.Length; k++) hra[k] = 0;
					for (k = 0; k < kr; k++) {
						for (l = 0; l < hra.Length; l++) {
							if (vra [k] > hra [l]) {
								hra [l] = vra [k];
							}
						}
					}
					R = hra [0];
					for (k = 1; k < hra.Length; k++)
						if (R > hra [k])
							R = hra [k];
					//mean = mean / kr;
					//R = mean;


					/* Green */
					//mean = 0;
					//meang = 0;
					//G = 0;
				//	gg.Clear ();
					for(k=0; k<hra.Length; k++) hra[k] = 0;
					for (k = 0; k < kr; k++) {
						for (l = 0; l < hra.Length; l++) {
							if (vga [k] > hra [l]) {
								hra [l] = vga [k];
							}
						}
					}
					G = hra [0];
					for (k = 1; k < hra.Length; k++)
						if (G > hra [k])
							G = hra [k];

					/* Blue */
					//mean = 0;
				//	meang = 0;
					//B = 0;
				//	gg.Clear ();
				//for (k = 0; k < kb; k++) {
					//mean += vba [k];
				//	}
				//	mean = mean / kb;
				//	B = mean;

					for(k=0; k<hra.Length; k++) hra[k] = 0;
					for (k = 0; k < kr; k++) {
						for (l = 0; l < hra.Length; l++) {
							if (vba [k] > hra [l]) {
								hra [l] = vba [k];
							}
						}
					}
					B = hra [0];
					for (k = 1; k < hra.Length; k++)
						if (B > hra [k])
							B = hra [k];

					R -= halfmask;
					G -= halfmask;
					B -= halfmask;

					r = (int)(R*WB_R*WB_Norm);
					g = (int)(G*WB_G*WB_Norm);
					b = (int)(B*WB_B*WB_Norm);

					if (r > 510)
						r = 255;
					else
						r = r / 2;
					if (r < 0)
						r = 0;
					if (g > 510)
						g = 255;
					else
						g = r / 2;
					if (g < 0)
						g = 0;	
					if (b > 510)
						b = 255;
					else
						b = r / 2;
					if (b < 0)
						b = 0;
					bmp.SetPixel ((int)j, (int)i, Color.FromArgb (255, r, g, b));
					//ImageLineHelper.SetPixel(imk, (int)j, r, g, b);
				}
				//pngw.WriteRow (imk, (int)i);
			}
			Console.WriteLine ("\nSuperimposed. Finishing...");
			//Finish ();
			bmp.Save("out.png", ImageFormat.Png);
			Console.WriteLine ("End");
		}


		public void Finish()
		{
			pngw.End();
		}

	}
}

