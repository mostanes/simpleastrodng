using System;
using System.Collections.Generic;
using TIFFParse;
using System.Runtime.CompilerServices;

namespace DNGReader
{
	public class DNGPixmap /*: IPixelMatrix */
	{
		string filename;
		TiffFile tf;
		byte[] bompa;
		public uint Width, Height;
		uint x0, y0, x1, y1, pos0, szx=0, szy=0;
//		public bool[,] mask;

		public DNGPixmap(string fn)
		{
			filename = fn;
			tf = new TiffFile(fn);
			tf.TiffEPParse();
			int i;
			uint AApos = 0;
			uint AA1, AA2, AA3, AA4;
			ushort j;
			for (i=0; i<tf.IFDs.Count; i++)
			{
				for (j=0; j<tf.IFDs[i].Count; j++)
				{
					Tag t = tf.IFDs [i] [j];
					if (t.nr == TagNumber.ImageWidth)
						szx = t.vof;
					if (t.nr == TagNumber.ImageLength)
						szy = t.vof;
					if (t.nr == TagNumber.StripOffsets)
						pos0 = t.vof;
					if(t.nr == TagNumber.ActiveArea)
						AApos = t.vof;
				}
			}
			AA1 = tf.GetUInt32(AApos);
			AA2 = tf.GetUInt32(AApos+4);
			AA3 = tf.GetUInt32(AApos+8);
			AA4 = tf.GetUInt32(AApos+12);
			x0 = AA2;
			y0 = AA1;
			x1 = AA4;
			y1 = AA3;
			Width = x1-x0;
			Height = y1-y0;
			bompa = tf.ReadBytes(pos0, szy * szx + szy * szx / 2);
			//mask = null;

		}
		public DNGPixmap(TiffFile tif)
		{
			tif.TiffEPParse();
			tf = tif;
			int i;
			uint AApos = 0;
			uint AA1, AA2, AA3, AA4;
			ushort j;
			for (i=0; i<tf.IFDs.Count; i++)
			{
				/*
				Console.WriteLine("----");
				Console.WriteLine(tff.IFDs [i].Count);
				Console.WriteLine("+");
				*/
				for (j=0; j<tf.IFDs[i].Count; j++)
				{
					Tag t = tf.IFDs [i] [j];
					if (t.nr == TagNumber.ImageWidth)
						szx = t.vof;
					if (t.nr == TagNumber.ImageLength)
						szy = t.vof;
					if (t.nr == TagNumber.StripOffsets)
						pos0 = t.vof;
					if(t.nr == TagNumber.ActiveArea)
						AApos = t.vof;
					/*
					Console.WriteLine("{");
					Console.WriteLine(t.nr);
					Console.WriteLine(t.ft);
					Console.WriteLine(t.count);
					Console.WriteLine(t.vof);
					Console.WriteLine("}");
					*/
				}
			}
			AA1 = tf.GetUInt32(AApos);
			AA2 = tf.GetUInt32(AApos+4);
			AA3 = tf.GetUInt32(AApos+8);
			AA4 = tf.GetUInt32(AApos+12);
			x0 = AA2;
			y0 = AA1;
			x1 = AA4;
			y1 = AA3;
			Width = x1-x0;
			Height = y1-y0;
			bompa = tf.ReadBytes(pos0, szy * szx + szy * szx / 2);

		}

		#region IPixelMatrix implementation

		public Pixel64 this[uint x, uint y]
		{
			get
			{
				if (x < 0)
					return new Pixel64 (new Color64 (7, 0, 0, 0), x, y);
				if (y < 0)
					return new Pixel64 (new Color64 (7, 0, 0, 0), x, y);
				if (x >= Width)
					return new Pixel64 (new Color64 (7, 0, 0, 0), x, y);
				if (y >= Height)
					return new Pixel64 (new Color64 (7, 0, 0, 0), x, y);

				x += x0;
				y += y0;
				x = (x / 2) * 2;
				y = (y / 2) * 2;
				uint pk1 = (y * szx + x) + (y * szx + x)/2;
				uint pk2 = ((y+1) * szx + x) + ((y+1) * szx + x)/2;
				ushort psar1, psar2, psar3, alpha;
				alpha = 0;
				psar1 = (ushort)(bompa[pk1]*16 + bompa[pk1+1] / 16);
				if (!ImageMask.mask [x-x0, y-y0]) {
					alpha += 1;
				}
				psar2 = (ushort)((bompa[pk1+1] % 16) * 256 + bompa[pk1+2]);
				if (!ImageMask.mask [x + 1-x0, y-y0])
					psar2 = 0;
				if(ImageMask.mask[x-x0, y+1-y0])
					psar2 += (ushort)(bompa[pk2]*16 + bompa[pk2+1] / 16);
				if (ImageMask.mask [x + 1-x0, y-y0] && ImageMask.mask [x-x0, y + 1-y0])
					psar2 = (ushort)(psar2 / 2);
				if (!ImageMask.mask [x + 1-x0, y-y0] && !ImageMask.mask [x-x0, y + 1-y0])
					alpha += 2;
				psar3 = (ushort)((bompa[pk2+1] % 16) * 256 + bompa[pk2+2]);
				if (!ImageMask.mask [x + 1-x0, y + 1-y0]) {
					alpha += 4;
				}
				return new Pixel64(new Color64(alpha, psar1,psar2,psar3), x, y);
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		#endregion

		//[MethodImpl(MethodImplOptions.)]
		public double GetPVal(uint x, uint y)
		{
			x += x0;
			y += y0;
			x = (x / 2) * 2;
			y = (y / 2) * 2;
			uint pk1 = (y * szx + x) + (y * szx + x)/2;
			uint pk2 = ((y+1) * szx + x) + ((y+1) * szx + x)/2;
			ushort psar1, psar2, psar3;
			psar1 = (ushort)(bompa[pk1]*16 + bompa[pk1+1] / 16);
			psar2 = (ushort)((bompa[pk1+1] % 16) * 256 + bompa[pk1+2]);
			psar2 += (ushort)(bompa[pk2]*16 + bompa[pk2+1] / 16);
			psar2 = (ushort)(psar2 / 2);
			psar3 = (ushort)((bompa[pk2+1] % 16) * 256 + bompa[pk2+2]);
			return (double)(psar1 + psar2 + psar3);
		}
		//[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public double GetPure(uint x, uint y)
		{
			x += x0;
			y += y0;
			uint pk = (y * szy + x) + (y * szy + x) / 2;
			ushort psar;
			if ((y * szy + x) % 2 == 0) {
				psar = (ushort)(bompa [pk] * 16 + bompa [pk + 1] / 16);
			} else {
				psar = (ushort)((bompa [pk] % 16) * 256 + bompa [pk + 1]);
			}
			return (double)psar;
		}
		//[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public ColorStar GetColorStar(uint x, uint y)
		{
			if (x < 0)
				return new ColorStar (0, 0, 0, 0, 0, 0);
			if (y < 0)
				return new ColorStar (0, 0, 0, 0, 0, 0);
			if (x >= Width)
				return new ColorStar (0, 0, 0, 0, 0, 0);
			if (y >= Height)
				return new ColorStar (0, 0, 0, 0, 0, 0);

			x += x0;
			y += y0;
			x = (x / 2) * 2;
			y = (y / 2) * 2;
			uint pk1 = (y * szx + x) + (y * szx + x)/2;
			uint pk2 = ((y+1) * szx + x) + ((y+1) * szx + x)/2;
			ushort psar1, psar2, psar3;
			double psp1, psp2, psp3;
			double ar, ag, ab;
			psar1 = (ushort)(bompa[pk1]*16 + bompa[pk1+1] / 16);
			if (!ImageMask.mask [x - x0, y - y0])
				ar = 0;
			else
				ar = 1;
			psar2 = (ushort)((bompa[pk1+1] % 16) * 256 + bompa[pk1+2]);
			if (!ImageMask.mask [x + 1-x0, y-y0])
				psar2 = 0;
			if(ImageMask.mask[x-x0, y+1-y0])
				psar2 += (ushort)(bompa[pk2]*16 + bompa[pk2+1] / 16);
			if (ImageMask.mask [x + 1-x0, y-y0] && ImageMask.mask [x-x0, y + 1-y0])
				psar2 = (ushort)(psar2 / 2);
			if (!ImageMask.mask [x + 1 - x0, y - y0] && !ImageMask.mask [x - x0, y + 1 - y0])
				ag = 0;
			else
				ag = 1;
			psar3 = (ushort)((bompa[pk2+1] % 16) * 256 + bompa[pk2+2]);
			if (!ImageMask.mask [x + 1 - x0, y + 1 - y0])
				ab = 0;
			else
				ab = 1;
			psp1 = (double)psar1;
			psp2 = (double)psar2;
			psp3 = (double)psar3;
			psp1 -= ImageMask.rem [x - x0, y - y0];
			psp2 -= (ImageMask.rem [x + 1 - x0, y - y0] + ImageMask.rem [x - x0, y + 1 - y0]) / 2;
			psp3 -= ImageMask.rem [x + 1 - x0, y + 1 - y0];
			return new ColorStar (ar, ag, ab, psp1, psp2, psp3);
		}
	}

}

