using System;
using System.IO;
using System.Collections.Generic;

namespace TIFFParse
{
	public class TiffFile
	{
		FileStream fs;
		public bool BOM;
		public List<IFD> IFDs;
		public UInt16 GetUInt16(UInt32 pos)
		{
			byte[] bop = new byte[2];
			fs.Seek(pos, SeekOrigin.Begin);
			fs.Read(bop, 0, 2);
			if (BOM == false)
			{
				return ((UInt16)(((UInt16)bop [0]) + ((UInt16)(((UInt16)bop [1]) * 256))));
			} else
			{
				return ((UInt16)(((UInt16)bop [1]) + ((UInt16)(((UInt16)bop [0]) * 256))));
			}
		}
		public UInt32 GetUInt32(UInt32 pos)
		{
			byte[] bop = new byte[4];
			fs.Seek(pos, SeekOrigin.Begin);
			fs.Read(bop, 0, 4);
			if (BOM == false)
			{
				return ((UInt32)((UInt32)(bop [0] + 256 * bop [1] + 65536 * bop [2]) + ((UInt32)bop [3]) * 256 * 65536));
			} else
			{
				return ((UInt32)((UInt32)(bop [3] + 256 * bop [2] + 65536 * bop [1]) + ((UInt32)bop [0]) * 256 * 65536));
			}
		}
		public byte[] ReadBytes(UInt32 pos, UInt32 size)
		{
			byte[] bop = new byte[size];
			fs.Seek(pos, SeekOrigin.Begin);
			fs.Read(bop, 0, (int)size);
			return bop;
		}
		public TiffFile(string filename)
		{
			fs = File.Open(filename, FileMode.Open);
			byte[] init = new byte[2];
			fs.Read(init, 0, 2);
			if (init [0] == 0x49)
			{
				if (init [1] != 0x49)
					throw new WrongFileFormatException("Byte order mark invalid. Corrupt file or wrong file type!");
				BOM = false;
			}
			if (init [0] == 0x4D)
			{
				if (init [1] != 0x4D)
					throw new WrongFileFormatException("Byte order mark invalid. Corrupt file or wrong file type!");
				BOM = true;
			}
			if (GetUInt16(2) != 42)
				throw new WrongFileFormatException("File signature wrong. Corrupt file or wrong file type!");
			UInt32 addr;
			addr = GetUInt32(4);
			IFDs = new List<IFD>();
			while (addr != 0)
			{
				IFD dfi = new IFD(this, addr);
				addr = dfi.NextIFD;
				IFDs.Add(dfi);
			}
		}
		public void TiffEPParse()
		{
			int i;
			for (i=0; i<IFDs.Count; i++)
			{
				Tuple<bool,IFD> dif = IFDs [i].GetSubIFD();
				if (dif.Item1)
				{
					IFDs.Add(dif.Item2);
				}
			}
		}
	}
	
}
