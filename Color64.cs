using System;

namespace DNGReader
{
	public struct Color64
	{
		public UInt16 A;
		public UInt16 R;
		public UInt16 G;
		public UInt16 B;
		public Color64(UInt16 a, UInt16 r, UInt16 g, UInt16 b)
		{
			A = a;
			R = r;
			G = g;
			B = b;
		}
		public Color64(UInt16[] uints)
		{
			if (uints.Length != 4)
				throw new ArgumentException("Wrong number of colors");
			A = uints[0];
			R = uints[1];
			G = uints[2];
			B = uints[3];
		}
		private static ushort GetUShort(byte[] ush, int pos, bool byteorder)
		{
			if (byteorder)
				return (ushort)(ush[pos] * 256 + ush[pos+1]);
			else
				return (ushort)(ush[pos] + ush[pos+1] * 256);
		}
		public Color64(byte[] col, bool byteorder)
		{
			bool f = false;
			A = 0; R = 0; G = 0; B = 0;
			if (col.Length == 4)
			{
				if (col[0] == 255)
					A = 65535;
				else
					A = (ushort)(col[0] * 256);
				R = (ushort)(col[1] * 256);
				G = (ushort)(col[2] * 256);
				B = (ushort)(col[3] * 256);
				f = true;
			}
			if (col.Length == 3)
			{
				A = 65535;
				R = (ushort)(col[0]*256);
				G = (ushort)(col[1]*256);
				B = (ushort)(col[2]*256);
				f = true;
			}
			if (col.Length == 8)
			{
				A = GetUShort(col, 0, byteorder);
				R = GetUShort(col, 2, byteorder);
				G = GetUShort(col, 4, byteorder);
				B = GetUShort(col, 6, byteorder);
				f = true;
			}
			if (col.Length == 6)
			{
				A = 65535;
				R = GetUShort(col, 0, byteorder);
				G = GetUShort(col, 2, byteorder);
				B = GetUShort(col, 4, byteorder);
				f = true;
			}
			if (f == false)
			{
				throw new ArgumentException("Wrong arguments");
			}
		}
		public double Value
		{
			get
			{
				return R + B + G;
			}
		}
	}
	public struct Pixel64
	{
		public Color64 color;
		public UInt32 x;
		public UInt32 y;
		public Pixel64(Color64 c, UInt32 X, UInt32 Y)
		{
			color = c;
			x = X;
			y = Y;
		}
	}
	public interface IPixelMatrix
	{
		Pixel64 this[UInt32 x, UInt32 y]
		{
			get;
			set;
		}
		UInt32 Height
		{
			get;
		}
		UInt32 Width
		{
			get;
		}
	}
	public class IBitmap64
	{
		IPixelMatrix pxm;
		UInt32 height;
		UInt32 width;
	}



	public struct ColorStar
	{
		public double AR, AG, AB;
		public double R, G, B;
		public ColorStar(double ar, double ag, double ab, double r, double g, double b)
		{
			AR = ar;
			AG = ag;
			AB = ab;
			R = r;
			G = g;
			B = b;
		}
		public double Value
		{
			get
			{
				return 3*(AR*R + AB*B + AG*G)/(AR+AG+AB);
			}
		}
	}
}

