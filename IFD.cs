using System;

namespace TIFFParse
{
	public enum FieldType : byte
	{
		Byte = 1,
		Ascii = 2,
		Short = 3,
		Long = 4,
		Rational = 5,
		Sbyte = 6,
		Undefined = 7,
		Sshort = 8,
		Slong = 9,
		Srational = 10,
		Float = 11,
		Double = 12
	}
	public struct Tag
	{
		public TagNumber nr;
		public FieldType ft;
		public UInt32 count;
		public UInt32 vof;
	}

	public struct IFD
	{
		TiffFile tiff;
		public UInt16 Count;
		public UInt32 BaseAddr;
		public UInt32 NextIFD;

		public IFD(TiffFile tf, UInt32 pos)
		{
			tiff = tf;
			BaseAddr = pos;
			Count = tiff.GetUInt16(BaseAddr);
			NextIFD = tiff.GetUInt32(BaseAddr + 12 * ((UInt32)Count));
		}
		public Tag this [UInt16 k]
		{
			get
			{
				UInt32 x = (UInt32) k;
				if(k >= Count)
					throw new ArgumentOutOfRangeException("x");
				Tag t = new Tag();
				t.nr = (TagNumber)tiff.GetUInt16(BaseAddr + 2 + 12 * x);
				t.ft = (FieldType)tiff.GetUInt16(BaseAddr + 12 * x + 4);
				t.count = tiff.GetUInt32(BaseAddr + 12 * x + 6);
				t.vof = tiff.GetUInt32(BaseAddr + 12 * x + 10);
				return t;
			}
		}
		public Tuple<bool,IFD> GetSubIFD()
		{
			IFD dif = new IFD();
			ushort k;
			for (k = 0; k < this.Count; k++)
			{
				Tag t = this [k];
				if (t.nr == TagNumber.SubIFDs)
				{
					dif = new IFD(this.tiff, t.vof);
					return new Tuple<bool, IFD>(true, dif);
				}
			}
			return new Tuple<bool, IFD>(false,dif);
		}
	}
}

