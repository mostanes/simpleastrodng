using System;

namespace TIFFParse
{
	public class WrongFileFormatException : Exception
	{
		public WrongFileFormatException() : base()
		{
		}
		public WrongFileFormatException(string message) : base(message)
		{
		}
	}
}

namespace DNGReader
{
	public class UnsupportedFileException : Exception
	{
		public UnsupportedFileException() : base("File is not supported.\n" +
		                                         "Please note that DNGReader only supporting a subset of the DNG/TIFF standard.")
		{
		}
	}
}
