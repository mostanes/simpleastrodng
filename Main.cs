using System;
using System.Collections.Generic;
using TIFFParse;
using System.IO;

namespace DNGReader
{
	class Program
	{
		static void Main(string[] args)
		{
			/* Process arguments */
			if (args.Length != 1) {
				Console.WriteLine ("Wrong number of arguments.");
			}
			FileStream fs;
			try {
				fs = File.Open (args [0], FileMode.Open);
			} catch {
				Console.WriteLine ("Could not open: " + args [0]);
				return;
			}
			string[] blacks;
			string[] whites;
			string[] process;
			string conf, c1;
			TextReader tr = new StreamReader (fs);
			conf = tr.ReadToEnd ();
			/* Process config file */
			int si, so;
			try {
				si = conf.IndexOf ('{');
				so = conf.IndexOf ('}', si);
				c1 = conf.Substring (si + 1, so - si - 2);
				blacks = c1.Split ('\n', '\r');
				si = conf.IndexOf ('{', so);
				so = conf.IndexOf ('}', si);
				c1 = conf.Substring (si + 1, so - si - 2);
				whites = c1.Split ('\n', '\r');
				si = conf.IndexOf ('{', so);
				so = conf.IndexOf ('}', si);
				c1 = conf.Substring (si + 1, so - si - 2);
				process = c1.Split ('\n', '\r');
			} catch {
				Console.WriteLine ("Wrong config file");
				return;
			}

			/* Process images */
			List<DNGPixmap> dps = new List<DNGPixmap>(process.Length);
			int i;
			for (i = 0; i < process.Length; i++) {
				if (process [i] == "")
					continue;
				dps.Add(new DNGPixmap (process [i]));
			}
			List<DNGPixmap> blacky = new List<DNGPixmap> (blacks.Length);
			List<DNGPixmap> whity = new List<DNGPixmap> (whites.Length);
			for (i = 0; i < blacks.Length; i++) {
				if (blacks [i] == "")
					continue;
				blacky.Add (new DNGPixmap (blacks [i]));
			}
			for (i = 0; i < whites.Length; i++) {
				if (whites [i] == "")
					continue;
				whity.Add (new DNGPixmap (whites [i]));
			}
			Masker m = new Masker ();
			m.MakePixmap (blacky, whity);
			Console.WriteLine ("Dark current values: " + ImageMask.dark_cu + " " + ImageMask.dark_cu_delta);
			AlignTransform at = new AlignTransform ();
			at.RunOverSet (dps.ToArray());
			SuperImpose sip = new SuperImpose (dps [0].Width, dps [0].Height, "output.png");
			sip.pxms = new List<DNGPixmap> (dps);
			sip.trns = at.trt;
			sip.StartSuperImposing ();
			Console.WriteLine ("::");

		}
	}
}
