﻿using System;
using System.Collections.Generic;

namespace DNGReader
{
	public static class ImageMask
	{
		public static bool[,] mask;
		public static double dark_cu;
		public static double dark_cu_delta;
		public static double[,] rem;

	}
	public class Masker
	{
		uint Width, Height;
		public bool[,] mask;
		const int OverVal = 300;
		const int UnderVal = 3000;
		public Masker ()
		{
		}

		public void MakePixmap(List<DNGPixmap> bl, List<DNGPixmap> wh)
		{
			Console.WriteLine ("Computing light and dark...");
			Width = bl [0].Width;
			Height = bl [0].Height;
			ImageMask.rem = new double[Width, Height];
			uint i, j;
			int k;
			double kom=0;
			double aruma=0, aruna=0;
			mask = new bool[Width, Height];
			Console.WriteLine ("Computing darkness:");
			DNGPixmap[] blacks = bl.ToArray ();
			DNGPixmap[] whites = wh.ToArray ();
			for (i = 0; i < Width; i++) {
				for (j = 0; j < Height; j++) {
					kom = 0;
					for (k = 0; k < blacks.Length; k++) {
						kom += blacks [k].GetPure (i, j);
					}
					if (kom > OverVal * k)
						mask [i, j] = false;
					else
						mask [i, j] = true;
					ImageMask.rem [i, j] = 3*kom/4/bl.Count;
					if (kom < OverVal * k) {
						aruma += kom;
						aruna += kom * kom;
					}
				}
				if (i % 100 == 0)
					Console.Write (i.ToString () + " ");

			}
			Console.WriteLine ("\nComputed darkness.");
			aruma = aruma / Width / Height / bl.Count;
			aruna = aruna / Width / Height / bl.Count;
			aruna = aruna - aruma * aruma;
			ImageMask.dark_cu = aruma;
			ImageMask.dark_cu_delta = Math.Sqrt(aruna);
			Console.WriteLine ("Computing lightness...");
			for (i = 0; i < Width; i++) {
				for (j = 0; j < Height; j++) {
					//kom = wh [0].GetPure (i, j);
					for (k = 0; k < whites.Length; k++) {
						kom += whites [k].GetPure (i, j);
					}
					if (kom < UnderVal * k)
						mask [i, j] = false;
				}
				if (i % 100 == 0)
					Console.Write (i.ToString () + " ");

			}
			Console.WriteLine ("\nComputed lightness.");
			ImageMask.mask = mask;
			Console.WriteLine ("Computed light and dark.");
		}
	}
}

