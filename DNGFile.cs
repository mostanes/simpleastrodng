using System;
using TIFFParse;
using System.Collections.Generic;

namespace DNGReader
{
	public class DNGFile : TIFFParse.TiffFile
	{
		Dictionary<TagNumber, Tag> CentralTags;
		public DNGFile(string filename) : base(filename)
		{
		}

		public void ParseDNG()
		{
			base.TiffEPParse();
			if (IFDs.Count != 2)
			{
				throw new UnsupportedFileException();
			}
			ushort i;
			Tag t;
			for (i=0; i<IFDs[1].Count; i++)
			{
				t = IFDs [1] [i];
				CentralTags.Add(t.nr, t);
			}
			try
			{
				t = CentralTags [TagNumber.NewSubfileType];
			} catch (KeyNotFoundException)
			{
				throw new WrongFileFormatException("File does not respect DNG standards");
			}
			if (t.count != 1)
				throw new WrongFileFormatException("File does not respect TIFF standards");
			if (t.ft != FieldType.Long)
				throw new WrongFileFormatException("File does not respect TIFF standards");
			if (t.vof != 1)
				throw new UnsupportedFileException();
			try
			{
				t = CentralTags [TagNumber.PhotometricInterpretation];
			} catch (KeyNotFoundException)
			{
				throw new WrongFileFormatException("File does not respect DNG standards");
			}
			if (t.count != 1)
				throw new WrongFileFormatException("File does not respect TIFF standards");
			if (t.vof % 65536 != 32803)
			{
				if (t.vof % 65536 != 34892)
					throw new WrongFileFormatException("File does not respect DNG standards");
				else
					throw new UnsupportedFileException();
			}
			try
			{
				t = CentralTags [TagNumber.BitsPerSample];
			} catch (KeyNotFoundException)
			{
				throw new WrongFileFormatException("File does not respect TIFF standards");
			}
			/* TODO: throw or support */
			if(t.vof % 65536 !=  12)
				throw new UnsupportedFileException();
			try
			{
				t = CentralTags [TagNumber.SampleFormat];
			} catch (KeyNotFoundException)
			{
				throw new WrongFileFormatException("File does not respect TIFF standards");
			}
			/* TODO: throw or support */
			if(t.vof % 65536 != 1)
				throw new UnsupportedFileException();
			try
			{
				t = CentralTags [TagNumber.Compression];
			} catch (KeyNotFoundException)
			{
				throw new WrongFileFormatException("File does not respect TIFF standards");
			}
			/* TODO: throw or support */
			if(t.vof != 1)
				throw new UnsupportedFileException();
			/* TODO: orientation */
		}
	}
}


