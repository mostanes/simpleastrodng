﻿using System;
using System.Collections.Generic;

namespace DNGReader
{
	public struct Peak
	{
		public int x;
		public int y;
		public int a_m_radius;
		public double valmax;
	}

	public struct Vector
	{
		public int xa, xb;
		public int ya, yb;
	}

	public struct Transform
	{
		public int delta_x;
		public int delta_y;
		public double a;
		public double b;

		public Vector TransformVector(Vector v)
		{
			Vector van;
			int px, pxp, py, pyp;
			van.xa = v.xa + delta_x;
			van.ya = v.ya + delta_y;
			px = v.xb - v.xa;
			py = v.yb - v.ya;
			pxp = (int)(a*px-b*py);
			pyp = (int)(b*px+a*py);
			van.xb = van.xa+pxp;
			van.yb = van.ya+pyp;
			return van;
		}

		public Transform(Vector v1, Vector v2)
		{
			delta_x = v2.xa - v1.xa;
			delta_y = v2.ya - v1.ya;
			int px, py, pxp, pyp;
			px = v1.xb - v1.xa;
			py = v1.yb - v1.ya;
			pxp = v2.xb - v2.xa;
			pyp = v2.yb - v2.ya;
			a = ((double)(px*pxp+py*pyp))/((double)(px*px+py*py));
			b = ((double)(px*pyp-py*pxp))/((double)(px*px+py*py));
		}
	}

	public class AlignTransform
	{
		DNGPixmap ipm;
		bool[,] bop;
		const int FDiscrimM = 5;
		const int FDiscrimS = 4;
		const int TransErr = 8;
		const int MaxNAStars = 2;
		const int MaxOORStars = 2;
		double dem;
		uint szx, szy;
		public List<Transform> trt;
		public List<List<Peak>> lop;

		public AlignTransform()
		{
		}

		public bool[,] Burp()
		{
			return bop;
		}

		public void RunOverSet(DNGPixmap[] dps)
		{
			lop = new List<List<Peak>> (dps.Length);
			int i;
			Console.WriteLine ("Counting stars...");
			for (i = 0; i < dps.Length; i++) {
				lop.Add (FindPeaks (dps [i]));
				Console.Write(lop [i].Count.ToString() + " ");
			}
			Console.WriteLine ("\nCounted stars");
			Console.WriteLine ("Calculating transformations...");
			Transformator ();
			Console.WriteLine ("Calculated");
		}

		public void Transformator()
		{
			//			List<Peak> a = lop [0];
			//List<Peak> b;
			int i;
			trt = new List<Transform> (lop.Count);
			Transform t0 = new Transform ();
			t0.a = 1; t0.b = 0; t0.delta_x = 0; t0.delta_y = 0;
			trt.Add (t0);
			for (i = 1; i < lop.Count; i++) {
				Pertrans (i);
			}
		}
		public void Pertrans(int i)
		{
			List<Peak> a, b;
			Vector av, bv;
			a = lop [0];
			b = lop [i];
			int ai, bi, api, bpi;
			for (ai = 0; ai < a.Count; ai++) {
				for (bi = 0; bi < a.Count; bi++) {
					if (bi == ai)
						continue;
					if ((a [bi].x - a [ai].x) + (a [bi].y - a [ai].y) < (ipm.Width + ipm.Height) / 4)
						continue;
					for (api = 0; api < b.Count; api++) {
						for (bpi = 0; bpi < b.Count; bpi++) {
							if (bpi == api)
								continue;
							av.xa = a [ai].x;
							av.ya = a [ai].y;
							av.xb = a [bi].x;
							av.yb = a [bi].y;
							bv.xa = b [api].x;
							bv.ya = b [api].y;
							bv.xb = b [bpi].x;
							bv.yb = b [bpi].y;
							Transform t = new Transform (av, bv);
							/*if ((t.a - 1) * (t.a - 1) > 0.1)
								continue;
							if (t.b * t.b > 0.1)
								continue;*/
							if (TestTransform (t, a, b)) {
								trt.Add (t);
								Console.WriteLine(t.a.ToString () + " " + t.b.ToString () + " " + t.delta_x.ToString () + " " + t.delta_y.ToString ());
								return;
							}
						}
					}
				}
			}
		}

		public bool TestTransform(Transform t, List<Peak> a, List<Peak> b)
		{
			int ai, bi;
			int mxe=0, moor=0;
			Vector av, ted;
			bool f;
			for(ai = 0; ai<a.Count; ai++)
			{
				av.xa = 0;
				av.ya = 0;
				av.xb = a [ai].x;
				av.yb = a [ai].y;
				ted = t.TransformVector (av);
				if (ted.xb < 0) {
					moor++;
					continue;
				}
				if (ted.yb < 0) {
					moor++;
					continue;
				}
				if (ted.xb > ipm.Width) {
					moor++;
					continue;
				}
				if (ted.yb > ipm.Height) {
					moor++;
					continue;
				}
				f = false;
				for(bi = 0; bi < b.Count; bi++)
				{
					if((((ted.xb - b[bi].x)*(ted.xb - b[bi].x) < TransErr*TransErr) && ((ted.yb - b[bi].y)*(ted.yb - b[bi].y) < TransErr*TransErr)))
					{
						f = true;
						break;
					}
				}
				if (!f)
					mxe++;
				if (mxe > MaxNAStars)
					return false;
				if (moor > MaxOORStars)
					return false;
			}
			return true;
		}

		public List<Peak> FindPeaks(DNGPixmap pixm)
		{
			List<Peak> loc = new List<Peak>();
			ipm = pixm;
			bop = new bool[ipm.Width,ipm.Height];
			dem = 0;
			//Color64 cx, cy, cd;
			uint i, j;
			szy = ipm.Height;
			szx = ipm.Width;
			for (i = 0; i < szx; i++)
			{
				for (j = 0; j < szy; j++)
				{
					dem += ipm.GetPVal (i, j);
				}
			}
			dem = dem / ipm.Width / ipm.Height;
			for (i = 0; i < szx; i++)
			{
				for (j = 0; j < szy; j++)
				{
					if (bop[i, j] == true)
						continue;
					if (ipm.GetPVal (i, j) > FDiscrimM * dem)
					{
//						bop[i, j] = true;
						Peak pk = RunFill (i, j);
						if (pk.a_m_radius > 4) {
							loc.Add (pk);
						}
					}
				}
			}
			return loc;
		}

		private Peak RunFill(uint x, uint y)
		{
			Queue<Tuple<uint,uint>> qt = new Queue<Tuple<uint, uint>>();
			Queue<Tuple<uint,uint>> qo = new Queue<Tuple<uint, uint>>();
			UInt16 mxvx, mxvy;
			double maxv;
			UInt32 nx;
			double rcx, rcy, rdcx, rdcy, radius;
			rcx = 0; rcy = 0; rdcx = 0; rdcy = 0;
			maxv = 0;
			qt.Enqueue(new Tuple<uint, uint>(x, y));
			while (qt.Count > 0)
			{
				Tuple<uint,uint> tp = qt.Dequeue();
				double cval = ipm[tp.Item1, tp.Item2].color.Value; /* This must be changed */
				if (cval > maxv)
				{
					maxv = cval;
					mxvx = (ushort)tp.Item1;
					mxvy = (ushort)tp.Item2;
				}
				if (cval > FDiscrimS * dem)
				{
					if (bop[tp.Item1, tp.Item2])
						continue;
					bop[tp.Item1, tp.Item2] = true;
					qo.Enqueue(tp);
					rcx += tp.Item1;
					rcy += tp.Item2;
					if (tp.Item1 >= szx-1)
						continue;
					if (tp.Item2 >= szy-1)
						continue;
					if(tp.Item1 > 0)
						qt.Enqueue(new Tuple<uint, uint>(tp.Item1 - 1, tp.Item2));
					if (tp.Item2 > 0)
						qt.Enqueue(new Tuple<uint, uint>(tp.Item1, tp.Item2 - 1));
					if (tp.Item2 < szy)
						qt.Enqueue(new Tuple<uint, uint>(tp.Item1, tp.Item2 + 1));
					if (tp.Item1 < szx)
						qt.Enqueue(new Tuple<uint, uint>(tp.Item1 + 1, tp.Item2));
				}
			}
			nx = (uint)qo.Count;
			rcx = rcx / nx;
			rcy = rcy / nx;
			while (qo.Count > 0)
			{
				Tuple<uint,uint> tp = qo.Dequeue();
				rdcx += (tp.Item1 - rcx) * (tp.Item1 - rcx);
				rdcy += (tp.Item2 - rcy) * (tp.Item2 - rcy);
			}
			radius = Math.Sqrt(2 * (rdcx + rdcy) / nx);
			Peak pk = new Peak();
			pk.a_m_radius = (int)Math.Floor(radius);
			maxv = (UInt16)Math.Floor(maxv);
			pk.valmax = maxv;
			pk.x = (int)Math.Floor(rcx);
			pk.y = (int)Math.Floor(rcy);
			return pk;
		}
	}
}

